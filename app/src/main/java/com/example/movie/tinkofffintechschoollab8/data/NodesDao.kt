package com.example.movie.tinkofffintechschoollab8.data

import androidx.lifecycle.LiveData
import androidx.room.*

@SkipQueryVerification
@Dao
interface NodesDao {
    @Insert
    fun insert(node: Node): Long

    @Delete
    fun delete(node: Node)

    @Query("DELETE FROM node WHERE id = :nodeId")
    fun deleteById(nodeId:Int)

    @Query("SELECT * FROM node")
    fun getAll(): LiveData<List<Node>>

    @Query("SELECT * FROM node WHERE id = :nodeId LIMIT 1")
    fun getNode(nodeId: Int): LiveData<Node>

    @Query("INSERT INTO node_relations VALUES (:parent, :child)")
    fun insertRelation(parent: Int, child: Int): Long

    @Query("DELETE FROM node_relations WHERE parent = :parent AND child = :child")
    fun deleteRelation(parent: Int, child: Int)

    @Transaction
    @Query("SELECT node.* FROM node INNER JOIN node_relations ON node.id = node_relations.parent " +
            "WHERE node_relations.child = :childId")
    fun getAllParents(childId: Int): LiveData<List<Node>>

    @Transaction
    @Query("SELECT node.* FROM node INNER JOIN node_relations ON node.id = node_relations.child " +
            "WHERE node_relations.parent = :parentId")
    fun getAllChildren(parentId: Int): LiveData<List<Node>>

}