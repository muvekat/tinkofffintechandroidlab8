package com.example.movie.tinkofffintechschoollab8

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.solver.widgets.ResolutionNode
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import com.example.movie.tinkofffintechschoollab8.data.Node
import com.example.movie.tinkofffintechschoollab8.data.NodesDao
import com.example.movie.tinkofffintechschoollab8.data.NodesDatabase
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    private lateinit var mListView: ListView
    private lateinit var mFab: FloatingActionButton
    private lateinit var mNodesDao: NodesDao
    private lateinit var mNodesAdapter: NodesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mListView = findViewById(R.id.listview)
        mFab = findViewById(R.id.addNode)
        mNodesDao = NodesDatabase.getInstance(this).nodesDao()

        mNodesAdapter = NodesAdapter(this)
        mListView.adapter = mNodesAdapter

        mNodesDao.getAll().observe(this, Observer {
            mNodesAdapter.clear()
            mNodesAdapter.addAll(it)
        })

        mFab.setOnClickListener {
            AddNodeDialogFragment().show(supportFragmentManager, "dialog")
        }
    }

    inner class NodesAdapter(context: Context) : ArrayAdapter<Node>(context, android.R.layout.simple_list_item_1) {
        private val mResource: Int = android.R.layout.simple_list_item_1

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var view = convertView
            if (view == null)
                view = layoutInflater.inflate(mResource , parent, false)

            val currentNode = this.getItem(position)
            if (currentNode != null) {
                view!!.findViewById<TextView>(android.R.id.text1).text = currentNode.value.toString()

                val currentId = currentNode.id
                zipLiveData(mNodesDao.getAllParents(currentId), mNodesDao.getAllChildren(currentId))
                    .observe(this@MainActivity, Observer {
                        val parentsList = it.first
                        val childrenList = it.second

                        if (!parentsList.isEmpty() && !childrenList.isEmpty()){
                            view.setBackgroundColor(resources.getColor(R.color.red))
                        } else if (!parentsList.isEmpty()){
                            view.setBackgroundColor(resources.getColor(R.color.blue))
                        } else if (!childrenList.isEmpty()){
                            view.setBackgroundColor(resources.getColor(R.color.yellow))
                        } else{
                            view.setBackgroundColor(resources.getColor(android.R.color.transparent))
                        }
                    })

                view.setOnClickListener {
                    val intent = Intent(this@MainActivity, EditNodeActivity::class.java)
                    intent.putExtra("NODE_ID", currentId)
                    startActivity(intent)
                }
            }


            return view!!
        }
    }

    class AddNodeDialogFragment: DialogFragment() {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val builder = AlertDialog.Builder(activity!!)
            val inflater = activity!!.layoutInflater

            builder.setView(inflater.inflate(R.layout.add_node_dialog, null))
                .setPositiveButton("Добавить") { dialog, _ ->

                    val inputString = (dialog as AlertDialog).findViewById<EditText>(R.id.number)!!.text

                    val enteredValue: Int
                    try{
                        enteredValue = inputString.toString().toInt()
                    } catch (e: NumberFormatException ){
                        return@setPositiveButton
                    }

                    thread {
                        NodesDatabase.getInstance(context!!).nodesDao().insert(Node(enteredValue))
                    }

                }

            return builder.create()
        }
    }
}
