package com.example.movie.tinkofffintechschoollab8

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.example.movie.tinkofffintechschoollab8.data.Node
import com.example.movie.tinkofffintechschoollab8.data.NodesDao
import com.example.movie.tinkofffintechschoollab8.data.NodesDatabase
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlin.concurrent.thread

class NodesFragment : Fragment() {

    private lateinit var mListView: ListView
    private lateinit var mNodesDao: NodesDao
    private lateinit var mNodesAdapter: NodesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.nodes_fragment_layout, container, false)

        mNodesDao = NodesDatabase.getInstance(context!!).nodesDao()

        arguments?.takeIf { it.containsKey("NODE_ID") && it.containsKey("PARENT_BOOLEAN") }?.apply {
            val nodeId = arguments!!.getInt("NODE_ID")
            val isParent = arguments!!.getBoolean("PARENT_BOOLEAN")

            mNodesAdapter = NodesAdapter(context!!, isParent, nodeId)
            mListView = view.findViewById(R.id.listview)

            mListView.adapter = mNodesAdapter


            val firstObservable = if (isParent) {
                mNodesDao.getAllParents(nodeId)
            } else {
                mNodesDao.getAllChildren(nodeId)
            }
            val secondObservable = mNodesDao.getAll()


            zipLiveData(firstObservable, secondObservable)
                .observe(this@NodesFragment, Observer { pair ->
                    val res = mutableListOf<Node>()
                    res.addAll(pair.first)
                    res.addAll(pair.second.filter { !res.contains(it) && it.id != nodeId })

                    mNodesAdapter.clear()
                    mNodesAdapter.addAll(res)
                })
        }

        return view
    }

    inner class NodesAdapter(context: Context, private val isParent: Boolean, private val nodeId: Int) :
        ArrayAdapter<Node>(context, android.R.layout.simple_list_item_1) {
        private val mResource: Int = android.R.layout.simple_list_item_1

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var view = convertView
            if (view == null)
                view = layoutInflater.inflate(mResource, parent, false)

            val currentNode = this.getItem(position)

            if (currentNode != null) {
                view!!.findViewById<TextView>(android.R.id.text1).text = currentNode.value.toString()

                val liveDataToObserve = if (this.isParent) {
                    mNodesDao.getAllParents(nodeId)
                } else {
                    mNodesDao.getAllChildren(nodeId)
                }

                liveDataToObserve.observe(this@NodesFragment, Observer {
                    if (it.contains(this.getItem(position)!!)) {
                        view.setBackgroundColor(resources.getColor(R.color.green))
                        view.tag = true
                    } else {
                        view.setBackgroundColor(resources.getColor(android.R.color.transparent))
                        view.tag = false
                    }
                })

                view.setOnClickListener {
                    thread {
                        if (it.tag == true) {
                            if (this.isParent)
                                mNodesDao.deleteRelation(currentNode.id, nodeId)
                            else
                                mNodesDao.deleteRelation(nodeId, currentNode.id)
                        } else {
                            if (this.isParent)
                                mNodesDao.insertRelation(currentNode.id, nodeId)
                            else
                                mNodesDao.insertRelation(nodeId, currentNode.id)
                        }
                    }
                }
            }


            return view!!
        }
    }
}