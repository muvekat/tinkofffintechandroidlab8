package com.example.movie.tinkofffintechschoollab8.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "node")
data class Node(
    val value: Int,
    @PrimaryKey(autoGenerate = true) var id:Int = 0
)