package com.example.movie.tinkofffintechschoollab8.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Node::class, NodeRelations::class], version = 1, exportSchema = false)
abstract class NodesDatabase: RoomDatabase(){
    abstract fun nodesDao(): NodesDao

    companion object {
        @Volatile
        private var INSTANCE: NodesDatabase? = null
        fun getInstance(context: Context): NodesDatabase {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }
        }
        private fun buildDatabase(context: Context): NodesDatabase =
            Room.databaseBuilder(
                context.applicationContext, NodesDatabase::class.java, "app-database"
            ).build()
    }
}