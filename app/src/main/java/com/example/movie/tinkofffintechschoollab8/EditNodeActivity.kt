package com.example.movie.tinkofffintechschoollab8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.movie.tinkofffintechschoollab8.data.Node
import java.nio.file.Files.size
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout


class EditNodeActivity : AppCompatActivity() {
    private var mNodeId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_node)

        mNodeId = intent.getIntExtra("NODE_ID", -1)

        if (mNodeId == -1)
            finish()

        val viewPager = findViewById<ViewPager>(R.id.viewpager)
        setupViewPager(viewPager)

        val tabLayout =  findViewById<TabLayout>(R.id.tabs)
        tabLayout.setupWithViewPager(viewPager)

    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        val parentsFragment = NodesFragment().apply {
            arguments = Bundle().apply { putInt("NODE_ID", mNodeId!!); putBoolean("PARENT_BOOLEAN", true) }
        }
        val childrenFragment = NodesFragment().apply {
            arguments = Bundle().apply { putInt("NODE_ID", mNodeId!!); putBoolean("PARENT_BOOLEAN", false) }
        }
        adapter.addFragment(parentsFragment, "Родители")
        adapter.addFragment(childrenFragment, "Дети")
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = mutableListOf<Fragment>()
        private val mFragmentTitleList = mutableListOf<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList.get(position)
        }
    }
}
