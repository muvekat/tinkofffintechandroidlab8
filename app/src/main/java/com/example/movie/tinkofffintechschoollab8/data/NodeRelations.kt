package com.example.movie.tinkofffintechschoollab8.data

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "node_relations",
    primaryKeys = ["parent", "child"],
    foreignKeys = [
        ForeignKey(entity = Node::class, parentColumns = ["id"], childColumns = ["parent"], onDelete = ForeignKey.CASCADE),
        ForeignKey(entity = Node::class, parentColumns = ["id"], childColumns = ["child"], onDelete = ForeignKey.CASCADE)
    ]
) data class NodeRelations(
    val parent: Int,
    val child: Int
)