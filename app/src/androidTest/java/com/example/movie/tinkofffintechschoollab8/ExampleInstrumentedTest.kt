package com.example.movie.tinkofffintechschoollab8

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer
import androidx.test.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import androidx.room.Room
import com.example.movie.tinkofffintechschoollab8.data.Node
import com.example.movie.tinkofffintechschoollab8.data.NodesDatabase

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @get:Rule
    val activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, true)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.example.movie.tinkofffintechschoollab8", appContext.packageName)
    }

    @Test
    fun testRoom(){
        val appContext = InstrumentationRegistry.getTargetContext()


    }
}
